= OpenStreetMap bearbeiten
OpenSchoolMaps.ch -- Freie Lernmaterialien zu freien Geodaten und Karten.
//
// HACK: suppress title page.
// See https://github.com/asciidoctor/asciidoctor-pdf/issues/95
ifdef::backend-pdf[:notitle:]
:lang: de
:figure-caption: Abbildung

ifdef::backend-pdf[]
[discrete]
= {doctitle}

{author}
endif::[]
// END OF suppress title page HACK

mit Hilfe des iD-Editors ein Bänkli, einen Brunnen oder einen Abfallkübel eintragen

*Ein Arbeitsblatt für Schülerinnen und Schüler*

NOTE: Mit diesem Arbeitsblatt und dem in den Online-OpenStreetMap-Editor "iD" eingebauten "Rundgang" (ein Software-Geführter Einführungskurs) lernst du, OpenStreetMap-Daten mit diesem Editor zu bearbeiten. So kannst du die Karte selbst verbessern und ergänzen.

Auf OpenStreetMap kannst du nicht nur die Weltkarte anschauen, sondern diese auch editieren. Um Daten zu ergänzen oder abzuändern, brauchst du zuerst ein Benutzerkonto.

== Benutzerkonto erstellen und anmelden
  
Um ein Benutzerkonto zu erstellen drückst du auf die Schaltfläche "Registrieren" oben rechts. Dazu musst du deine Email-Adresse angeben und dir einen Benutzername und ein Passwort aussuchen. Falls du bereits ein OpenStreetMap-Benutzerkonto hast, kannst du stattdessen natürlich direkt auf "Anmelden" klicken.

Nachdem du dich durch die Registrierung ganz durchgeklickt hast, schickt dir OpenStreetMap ein Email und die Adresse, die du angegeben hast, um zu prüfen, ob du wirklich über diese Email-Adresse erreichbar bist. In dieser Email-Nachricht wirst du einen Bestätigungs-Link finden, den du besuchen musst, um die Registrierung abzuschliessen.

== Sprache umstellen (falls nötig)

Wenn die Knöpfe auf openstreetmap.org nach der Anmeldung nicht in deutscher Sprache angezeigt werden, kannst du das folgendermassen ändern:

1. Klicke oben rechts auf den Knopf mit deinem Benutzernamen
2. Wähle im Dropdown-Menü "Settings" aus
3. Unter der Einstellung "Preferred Languages" ersetzt du den Text mit `de-CH de`
4. Klicke den "Save Changes"-Knopf ganz unten, um diese Änderung zu speichern

Der im Folgenden verwendete Editor wird die selbe Sprache verwenden, falls verfügbar.

== iD, der Web-Editor von OpenStreetMap

Es gibt verschiedene Editoren, mit denen OpenStreetMap bearbeitet werden kann. Wir werden den Editor namens "iD" verwenden, der auf der openstreetmap.org-Website bereits eingebaut ist.

=== Rundgang

Um diesen Editor kennenzulernen, solltest du zuerst dessen eingebauten "Rundgang" durcharbeiten. Dazu drückst du auf den "Bearbeiten"-Knopf oben links. Falls dir danach eine Willkommens-Nachricht angezeigt wird, drückst du auf "Rundgang starten". Andernfalls drückst du auf das Fragezeichen-Symbol (auf der rechten Seite) und startest von dort aus den Rundgang.

=== Und los geht's!

Alle Änderungen, die du während des "Rundgangs" gemacht hast, waren nur zur Übung und wurden nicht wirklich in OpenStreetMap eingetragen.

Fehlt auf der OpenStreetMap-Karte etwas, oder ist etwas gar falsch? Vielleicht fällt dir an deinem Wohnort oder in der Gegend deines Schulhauses etwas auf, das verbessert oder ergänzt werden kann. Gibt es z.B. ein Bänkli, einen Brunnen oder einen Abfalleimer, der noch fehlt?

Da du im Rundgang gelernt hast, wie du Dinge in OpenStreetMap eintragen und ändern kannst, kannst du nun fehlendes eintragen und falsches korrigieren!
Innerhalb von 5–30 Minuten werden deine Änderungen durchgeführt und auf die Karte angewendet!

[NOTE]
====
OpenStreetMap speichert Objekt-Eigenschaften in Form von so genannten Tags.
Diese siehst du auf der linken Seite im iD-Editor, nachdem du ein Objekt angewählt hast.

.{zwsp}
image::../../../bilder/osm_editieren/eigenschaften_zugeklappt.PNG["Eigenschaften zugeklappt", 295, 394]

Wenn du das Feld "Alle Eigenschaften" aufklappst, sieht du alle Tags des Objekts.

.{zwsp}
image::../../../bilder/osm_editieren/eigenschaften_aufgeklappt.PNG["Eigenschaften aufgeklappt"]

Wenn du nicht weisst, welchen Tag du verwenden sollst, kannst du im Cheatsheet nachschauen.
====

Falls dir selbst nichts aufgefallen ist, was geändert oder ergänzt werden müsste, klicke auf das "OpenStreetMap"-Logo oben links, um den iD-Editor zu verlassen und zur Karte zurückzukehren. Klicke dann auf den "Ebenen"-Knopf (der mit den gestapelten Flächen image:../../../bilder/osm_editieren/osm_ebene_icon.PNG["Flächen-Icon", 25, 25] als Icon) und wähle die Checkbox "Hinweise/Fehlermeldungen" an. Schau, ob du Notizen anderer Kartenbenutzer siehst, die auf Fehler oder Auslassungen hinweisen, die du durch Änderungen beheben kannst. Wenn du etwas gefunden hast, klicke wieder auf "Bearbeiten".